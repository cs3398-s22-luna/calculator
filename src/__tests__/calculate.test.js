import { expect } from 'chai';
import React from 'react';
import calculate from "../logic/calculate";

// test(["+", "2", "+", "5"], {
//         next: "5",
//         total: "2",
//         operation: "+",
//       });

describe('Calculate Tests', ()=>{
    test('Number button tests', () => {
        expect(calculate({
                total: null,
                next: null,
                operation: null},"4")).toHaveProperty('next', "4");
        expect(calculate({
                total: null,
                next: 1,
                operation: null},"1")).toHaveProperty('next', "11");
    });
    test('Addition button tests', () => {
        expect(calculate({
                total: null,
                next: null,
                operation: "+"},"1")).toHaveProperty('next', "1");

        expect(calculate({
                total: null,
                next: 1,
                operation: "+"},"1")).toHaveProperty('next', "11");
    });

    //Percent button tests
    test('Percent button tests',() =>{
            expect(calculate({
                    previous: null,
                    current: 69,
                    operation: null
            }, "%")).toHaveProperty('current',"0.69");
    });

    //Add +/-  tests
    test('+/- tests',()=> {
            expect(calculate({
                    previous: -100,
                    current: null,
                    operation: null
            },"+,-")).toHaveProperty('previous',"100")
 
    });

    //Add decimal button tests
    test('decimal (.) button test',() =>{
            expect(calculate({
                    previous: 24,
                    current: null,
                    operation: null
            },".")).toHaveProperty('current',"0.")
    })


});


